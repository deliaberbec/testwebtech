import React, {Component} from 'react'
import BookStore from './BookStore'
import {EventEmitter} from 'fbemitter'

const emitter= new EventEmitter()
const store= new BookStore(emitter)

class AuthorsBooks extends Component{
    constructor(props){
        super(props)
        this.state= {
            books: []
        }
        
    }
    
    componentDidMount(){
        store.getByAuthor(this.props.authorNameFromParent)
        emitter.addListener('AUTHOR BOOKS', ()=>{
            this.setState({books: store.content})
        })
        
    }
    
        
    render(){
        return (
            <div>
            {
            this.state.books.map((b)=>
            <div>{b.title +'('+b.year+')'}</div>
            )
            }
            </div>
        )
    }
}

export default AuthorsBooks