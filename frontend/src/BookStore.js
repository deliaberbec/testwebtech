import axios from 'axios'
const SERVER= 'http://deliawebtech-deliaberbec.c9users.io:8080'

class BookStore{
    constructor(ee){
        this.emitter= ee
        this.content= []
    }
    getAll(){
        axios(SERVER+ '/books')
        .then((response)=>{
            
        this.content= response.data
        this.emitter.emit('BOOK LOAD')
        })
        .catch((error)=>console.warn(error))
    }
    
    getNovels(){
        axios(SERVER+'/books/genres/novel')
        .then((response)=>{
            this.content=response.data
            this.emitter.emit('NOVELS LOAD')
        }).catch((error)=>console.warn(error))
    }
    
    getFiction(){
        axios(SERVER+'/books/genres/fiction')
        .then((response)=>{
            this.content=response.data
            this.emitter.emit('FICTION LOAD')
        }).catch((error)=>console.warn(error))
    }
    
    getCrime(){
        axios(SERVER+'/books/genres/crime')
        .then((response)=>{
            this.content=response.data
            this.emitter.emit('CRIME LOAD')
        }).catch((error)=>console.warn(error))
    }
    
    getByAuthor(authorName){
        axios(SERVER+'/books/authors/' +authorName)
        .then((response)=>{
            this.content=response.data
            this.emitter.emit('AUTHOR BOOKS')
        }).catch((error)=>console.warn(error))
    }
}

export default BookStore