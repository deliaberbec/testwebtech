import React, { Component } from 'react';
 // eslint-disable-next-line
import logo from './logo.svg';
import './App.css';
import Books from './Books'
import Search from './Search'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">ReadIT <img src={"book.png"} className="App-logo" alt="logo" /></h1>
        </header>
        
        <Search />
      </div>
    );
  }
}

export default App;
