import React, {Component} from 'react'
import BookList from './BookList'
import './Books.css'
import NovelsList from './NovelsList'
import FictionList from './FictionList'
import CrimeList from './CrimeList'


class Books extends Component{
    constructor(props){
        super(props)
        this.state={
            component: 1
        };
        
    }
    
    seeAllBooks() {
    this.setState({
      component: 1
    })
  }
    
    seeNovels() {
    this.setState({
      component: 2
    })
  }
  
    seeFiction(){
    this.setState({
      component: 3
      })
  }
  
  seeCrime(){
    this.setState({
      component: 4
      })
  }
  
    render() {
    return (
    
    <div>
        
        <div className="buttons">
        <input type="button" value="All Books" className="buttons" onClick={this.seeAllBooks.bind(this)}/>
        <input type="button" value="Novels" className="buttons" onClick={this.seeNovels.bind(this)}/>
        <input type="button" value="Fiction" className="buttons" onClick={this.seeFiction.bind(this)}/>
        <input type="button" value="Crime" className="buttons" onClick={this.seeCrime.bind(this)}/>
        </div>
      
      <br></br><br></br>
     
      <div className="lists">
        
        {this.state.component==1 && <BookList />}
        {this.state.component==2 && <NovelsList />}
        {this.state.component==3 && <FictionList />}
        {this.state.component==4 && <CrimeList />}

      </div>
      
      
    </div>
    );
  }
  
}

export default Books