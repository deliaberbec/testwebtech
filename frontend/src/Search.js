import React, { Component } from 'react';
import {EventEmitter} from 'fbemitter'
import { Row, Col, FormControl, Button } from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';
import Books from "./Books";
import "./Search.css";
import BookStore from './BookStore'
import AuthorsBooks from './AuthorsBooks'

let ee = new EventEmitter()
let store = new BookStore(ee)

class Search extends Component {
    constructor(props){
        super(props)
        this.state={
            component: 0,
            text: ""
        };
        
    }
 
 handleChange(event) {
    this.setState({text: event.target.value});
  }
  
  searchAuthorBooks(){
    this.setState({
      component: 1
    })
  }
  
  searchDone(){
    this.setState({
      component: 0
    })
  }
  
  render(){
   
    return (
      <div>
       <Books />
       
       <input type="text" id='searchText' placeholder="Search by Author" onChange={this.handleChange.bind(this)}/> 
       <input type="button" id="search" value="Search" onClick={this.searchAuthorBooks.bind(this)}/>
       <input type="button" id="searchDone" value="Done" onClick={this.searchDone.bind(this)}/>
     
     <br></br><br></br>
     <div className="authorsBooks">
     {this.state.component==1 && <AuthorsBooks authorNameFromParent={this.state.text} />}
     </div>
      </div>
      )
    
}
}

export default Search;