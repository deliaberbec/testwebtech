import React, {Component} from 'react'
import BookStore from './BookStore'
import {EventEmitter} from 'fbemitter'

const emitter= new EventEmitter()
const store= new BookStore(emitter)

class NovelsList extends Component{
    constructor(props){
        super(props)
        this.state= {
            books: []
        }
        
    }
    
    componentDidMount(){
        store.getNovels()
        emitter.addListener('NOVELS LOAD', ()=>{
            this.setState({books: store.content})
        })
        
    }
    
        
    render(){
        return (
            <div>
            {
            this.state.books.map((b)=>
            <div>{b.title +' by '+ b.author +'('+b.year+')'}</div>
            )
            }
            </div>
        )
    }
}

export default NovelsList