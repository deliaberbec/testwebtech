const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')


const sequelize = new Sequelize('rest_project','root','',{
  dialect : 'mysql',
  define : {
    timestamps : false
  }
})

//Defining the users table
const User = sequelize.define('user',{
  name : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,40]
    }
  },
  email : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      isEmail : true
    }
},

   password: {
       type: Sequelize.STRING,
       allowNull: false,
       validate: {
           len: [8, 30]
       }
   }
   },{
  underscored : true
})

//Defining the books table
const Book = sequelize.define('book', {
  title : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [3,40]
    }
  },
  author : {
    type : Sequelize.STRING,
    allowNull : false,
    validate : {
      len : [5,40]
    }
  },
  year : {
    type : Sequelize.INTEGER,
    allowNull : false,
  },
  genre:{
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
          len:[3, 20]
      },
  user_id : {
    type : Sequelize.INTEGER,
    allowNull : false,
  },
  content: Sequelize.TEXT
  }
    
},{
  underscored: true
})

Book.belongsTo(User, {foreignKey: 'user_id', targetKey: 'id'})

const app= express()

app.use(bodyParser.json())

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
  next()
})

//Creating the database
app.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})


//USER FUNCTIONS

//gets all the users
app.get('/users', (req, res, next) => {
  User.findAll()
    .then((users) => res.status(200).json(users))
    .catch((error) => next(error))
})

//adds an user
app.post('/users', (req, res, next) => {
  User.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

//gets an user with the specified id
app.get('/users/:id', (req, res, next) => {
  User.findById(req.params.id, {include : [Book]})
    .then((user) => {
      if (user){
        res.status(200).json(user)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

//updates data of a user
app.put('/users/:id', (req, res, next) => {
  User.findById(req.params.id)
    .then((user) => {
      if (user){
        return user.update(req.body, {fields : ['name', 'email', 'password', 'books']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')  
      }
    })
    .catch((error) => next(error))
})

//deletes user with the specified id
app.delete('/users/:id', (req, res, next) => {
  User.findById(req.params.id)
    .then((user) => {
      if (user){
        return user.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')  
      }
    })
    .catch((error) => next(error))
})

//adds a book to the user having the specified id
app.post('/users/:id/books', (req, res, next) => {
	let u
	User.findById(req.params.id)
		.then((user) => {
			if(user){
				u = user
				return Book.create(req.body)
			}
			else{
				res.status(404).send('not found')
			}})
		.then((book) => {
			u.addBook(book)
			res.status(201).send('created')
		})
		.catch((err) => next(err))

})

//gets all the books belonging to the user with the specified id
app.get('/users/:uid/books', (req, res, next) => {
  User.findById(req.params.uid)
    .then((user) => {
      if (user){
        return user.getBooks()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then((books) => {
      if (!res.headersSent){
        res.status(200).json(books)  
      }
    })
    .catch((error) => next(error))  
})

//BOOK FUNCTIONS

//gets all the books
app.get('/books', (req, res, next) => {
  Book.findAll()
    .then((books) => res.status(200).json(books))
    .catch((error) => next(error))
})

//gets all books that are the specified genre
app.get('/books/genres/:gen', (req, res, next) => {
  Book.findAll({where: {genre: req.params.gen}, raw: true})
    .then((books) => res.status(200).json(books))
    .catch((error) => next(error))
})

//gets all books belonging to the specified author
app.get('/books/authors/:authorname', (req, res, next) => {
  Book.findAll({where: {author: req.params.authorname}, raw: true})
    .then((books) => res.status(200).json(books))
    .catch((error) => next(error))
})

//gets book with specified id
app.get('/books/:id', (req, res, next) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (book){
        res.status(200).json(book)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((error) => next(error))
})

//adds a book
app.post('/books', (req, res, next) => {
  Book.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error))
})

//deletes book with the specified id
app.delete('/books/:id', (req, res, next) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (book){
        return book.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')  
      }
    })
    .catch((error) => next(error))
})

//modifies book with specified id
app.put('/books/:id', (req, res, next) => {
  Book.findById(req.params.id)
    .then((book) => {
      if (book){
        return book.update(req.body, {fields : ['title', 'author', 'year', 'genre']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')  
      }
    })
    .catch((error) => next(error))
})


app.listen(8080)